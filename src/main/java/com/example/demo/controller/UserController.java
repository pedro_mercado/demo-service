package com.example.demo.controller;

import com.example.demo.config.MyUserDetailsService;
import com.example.demo.entity.User;
import com.example.demo.model.AuthenticationRequest;
import com.example.demo.model.AuthenticationResponse;
import com.example.demo.entity.Role;
import com.example.demo.repository.RoleRepository;
import com.example.demo.spec.UserSpec;
import com.example.demo.service.UserService;
import com.example.demo.util.JwtUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.supercsv.io.*;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "api/v1/user")
public class UserController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    //dependency injection (replaces to @Autowired annotation)
    private final UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    private static final String TOTALENTRIES = "X-Total-Entries";
    private static final String ACCESSCONTROLEXPOSE = "access-control-expose-headers";

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> registration(@RequestBody User user){
        if(user.getRoles().isEmpty()){
            Optional<Role> defaultRole = roleRepository.findById(Long.valueOf(1));
            if(!defaultRole.isPresent()){
                throw new IllegalStateException("No role available.");
            }
            user.setRoles(Collections.singleton(defaultRole.get()));
        }

        String jwt = null;

        if(userService.registration(user).getId() != null){
            byte[] passDecoded = Base64.decodeBase64(new String(user.getPassword()));
            try {
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(user.getEmail(), new String(passDecoded))
                );
            } catch (BadCredentialsException e){
                throw new BadCredentialsException("Incorrect username or password", e);
            }

            final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
            jwt = jwtTokenUtil.generateToken(userDetails);
        }
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequest) throws BadCredentialsException {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e){
            throw new BadCredentialsException("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUserName());
        final String jwt = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/{offset}/{limit}/{sortBy}/{sortType}")
    public List<User> getUsersFilters(
            @RequestBody User filters,
            @PathVariable("offset") Long offset,
            @PathVariable("limit") Long limit,
            @PathVariable("sortBy") String sortBy,
            @PathVariable("sortType") String sortType,
            @RequestParam(name = "globalFilter", required = false) String globalFilter,
            @AuthenticationPrincipal UserDetails user,
            HttpServletResponse response){
        UserSpec spec = new UserSpec();
        if(globalFilter == null || globalFilter.equals("")){
            //Use conventional filter mechanism
            Page<User> page = userService.getUsers(spec.getFilter(filters), offset, limit, sortBy, sortType);
            response.addHeader(TOTALENTRIES, String.valueOf(page.getTotalElements()));
            response.setHeader(ACCESSCONTROLEXPOSE,TOTALENTRIES);
            return page.getContent();
        }else{
            //Use global filtering mechanism
            Page<User> page = userService.getUsers(spec.getGlobalFilter(globalFilter), offset, limit, sortBy, sortType);
            response.addHeader(TOTALENTRIES, String.valueOf(page.getTotalElements()));
            response.setHeader(ACCESSCONTROLEXPOSE,TOTALENTRIES);
            return page.getContent();
        }
    }

    @GetMapping("/email/{email}")
    public List<User> getUserByEmail(@PathVariable("email") String email){
        return userService.getUsersByEmail(email);
    }

    @PostMapping("/save")
    public void saveUser(@RequestBody User user, @AuthenticationPrincipal UserDetails userDetails){
        user.setPassword("temp");
        userService.saveUser(user, userDetails);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
    }

    @PutMapping("/update/{id}")
    public User updateUser(@PathVariable("id") Long id,
                              @RequestParam(required = false) String name,
                              @RequestParam(required = false) String email,
                           @AuthenticationPrincipal UserDetails userDetails){
        return userService.updateUser(id, name, email, userDetails);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/export/{sortBy}/{sortType}")
    @ResponseBody
    public void userReportCsv(
            @RequestBody User filters,
            @PathVariable("sortBy") String sortBy,
            @PathVariable("sortType") String sortType,
            @RequestParam(name = "globalFilter", required = false) String globalFilter,
            @AuthenticationPrincipal UserDetails user,
            @RequestParam(required = false) String[] columns,
            HttpServletResponse response) throws IOException {
        String csvFileName = "userReport.csv";
        response.setContentType("text/csv");

        // creates mock data
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
        response.setHeader(headerKey, headerValue);

        List<User> users;
        UserSpec spec = new UserSpec();
        if(globalFilter == null || globalFilter.equals("")){
            //Use conventional filter mechanism
            users = userService.exportUsers(spec.getFilter(filters), sortBy, sortType);
        }else{
            //Use global filtering mechanism
            users = userService.exportUsers(spec.getGlobalFilter(globalFilter), sortBy, sortType);
        }

        // uses the Super CSV API to generate CSV data from the model data
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

        String[] header = { "id", "name", "lastName", "username", "email", "dob", "role"};

        try{
            if(columns.length > 0){
                header = columns;
            }
        } catch (NullPointerException exception){

        }

        //if class has field same as the sortBy then create the sort.
        csvWriter.writeHeader(header);

        List<User> userList = new ArrayList<>();
        for (User obj: users) {
            obj.setRole(obj.getRoles().iterator().next().getRole());
            userList.add(obj);
            csvWriter.write(obj, header);
        }
        csvWriter.close();
    }
}
