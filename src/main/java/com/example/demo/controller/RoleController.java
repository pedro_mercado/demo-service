package com.example.demo.controller;

import com.example.demo.entity.Role;
import com.example.demo.model.RoleModel;
import com.example.demo.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping()
    public List<Role> getRoles(@AuthenticationPrincipal UserDetails user){
        return roleService.getRoles();
    }

    @GetMapping("/users")
    public List<RoleModel> getUsersByRole(@AuthenticationPrincipal UserDetails user) {
        return roleService.getRolesAndUsers(user);
    }

}
