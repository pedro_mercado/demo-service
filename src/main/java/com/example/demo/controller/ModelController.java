package com.example.demo.controller;

import com.example.demo.entity.Model;
import com.example.demo.model.ModelModel;
import com.example.demo.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "api/v1/model")
public class ModelController {
    @Autowired
    ModelService modelService;

    @GetMapping()
    public List<ModelModel> getAll(){
        return modelService.getAll();
    }

    @GetMapping("/{ids}")
    public List<Model> getModelsById(@PathVariable("ids") List<Long> ids){
        return modelService.getModelstById(ids);
    }
}
