package com.example.demo.controller;

import com.example.demo.service.AssetService;
import com.example.demo.entity.Asset;
import com.example.demo.model.AssetModel;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/asset")
public class AssetController {

    @Autowired
    private UserService userService;

    @Autowired
    private AssetService assetService;

    public AssetController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    private List<AssetModel>getAssets(@AuthenticationPrincipal UserDetails userDetails){
        return assetService.getAssets(userDetails);
    }

    @PostMapping("/register/{userId}")
    private void registerAsset(@RequestBody Asset asset, @PathVariable("userId") Long userId, @AuthenticationPrincipal UserDetails userDetails){
        assetService.registerAsset(asset, userId, userDetails);
    }
}
