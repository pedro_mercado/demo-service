package com.example.demo.util;

import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthorityValidator {
        public void valudateAdminAndSuperAdmin(UserDetails user){
            if(!user.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN")) && !user.getAuthorities().contains(new SimpleGrantedAuthority("SUPER ADMIN"))){
                throw new AuthorizationServiceException("", new Throwable("Not Allowed."));
            }
        }

    public void valudateSuperAdmin(UserDetails user) {
        if (!user.getAuthorities().contains(new SimpleGrantedAuthority("SUPER ADMIN"))) {
            throw new AuthorizationServiceException("", new Throwable("Not Allowed."));
        }
    }
}
