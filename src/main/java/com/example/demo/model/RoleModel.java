package com.example.demo.model;

import com.example.demo.entity.User;

import java.util.HashSet;
import java.util.Set;

public class RoleModel {
    private Long id;
    private String role;
    Set<User> users = new HashSet<>();

    public RoleModel() {
    }

    public RoleModel(Long id, String role, Set<User> users) {
        this.id = id;
        this.role = role;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
