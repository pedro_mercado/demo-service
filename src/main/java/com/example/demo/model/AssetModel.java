package com.example.demo.model;

import com.example.demo.entity.User;

public class AssetModel {
    private Long id;
    private String assetType;
    private String location;
    private String macAddress;
    private String ipAddress;
    private User user;

    public AssetModel() {
    }

    public AssetModel(Long id, String assetType, String location, String macAddress, String ipAddress, User user) {
        this.id = id;
        this.assetType = assetType;
        this.location = location;
        this.macAddress = macAddress;
        this.ipAddress = ipAddress;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
