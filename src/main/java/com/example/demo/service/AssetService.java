package com.example.demo.service;

import com.example.demo.repository.AssetRepository;
import com.example.demo.entity.Asset;
import com.example.demo.model.AssetModel;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.AuthorityValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AssetService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    AuthorityValidator authorityValidator;

    public List<AssetModel> getAssets(UserDetails userDetails) {
        authorityValidator.valudateAdminAndSuperAdmin(userDetails);
        List<AssetModel> assets = new ArrayList<>();
        AssetModel assetModel;
        for (Asset asset: assetRepository.findAll()) {
            assetModel = new AssetModel(asset.getId(), asset.getAssetType(), asset.getLocation(), asset.getMacAddress(), asset.getIpAddress(), new User(asset.getUser().getId(), asset.getUser().getName(), asset.getUser().getEmail(), asset.getUser().getDob()));
            assets.add(assetModel);
        }
        return assets;
    }

    public void registerAsset(Asset asset, Long userId, UserDetails userDetails){
        authorityValidator.valudateSuperAdmin(userDetails);
        User user = userRepository.findUserById(userId);
        asset.setUser(user);
        assetRepository.save(asset);
    }
}
