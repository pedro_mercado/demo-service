package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.entity.Role;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.AuthorityValidator;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.util.*;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityValidator authorityValidator;

    @Autowired
    private EntityManager em;

    Sort sortPage;
    Pageable pageable;

    public Page<User> getUsers(Specification<User> filters, Long offset, Long limit, String sortBy, String sortType){
        try {
            sortPage = ((sortType.equals("desc")) ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending());
            Class<User> obj = User.class;
            Field f = obj.getDeclaredField(sortBy);

            f.setAccessible(true);
            //if class has field same as the sortBy then create the sort.
            if(f.getName().equals(sortBy)){
                pageable = PageRequest.of((offset.intValue()/limit.intValue()), limit.intValue(), sortPage);
            }

        } catch (NoSuchFieldException | IllegalAccessError | NullPointerException | IllegalArgumentException exception){
            pageable = PageRequest.of((offset.intValue()/limit.intValue()), limit.intValue(), Sort.by("id").ascending());
        }
        return userRepository.findAll(filters, pageable);
    }

    public List exportUsers(Specification<User> filters, String sortBy, String sortType){
        try{
            Class<User> obj = User.class;
            Field f = obj.getDeclaredField(sortBy);
            f.setAccessible(true);
            //if class has field same as the sortBy then create the sort.
            if(f.getName().equals(sortBy)) {
                sortPage = ((sortType.equals("desc")) ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending());
            }
        }
        catch (PropertyReferenceException | NoSuchFieldException exception){
            sortPage = Sort.by("id").ascending();
        }
        return userRepository.findAll(filters, sortPage);
    }

    public void saveUser(User user, UserDetails userDetails){
        authorityValidator.valudateAdminAndSuperAdmin(userDetails);
        Optional<User> userByEmail = userRepository.findUserByEmail(user.getEmail());
        if(userByEmail.isPresent()){
            throw new IllegalStateException("Email already exists.");
        }
        else if(user.getEmail().isEmpty()){
            throw new IllegalStateException("Email cannot be null.");
        }
        else if(user.getPassword().isEmpty()){
            throw new IllegalStateException("Password cannot be null.");
        }
        user.setPassword(encodeToBase64(user.getPassword()));
        userRepository.save(user);
    }

    public List<User> getUsersByEmail(String email){
        return userRepository.getUsersByEmail(email);
    }

    public void deleteUser(Long id) {
        boolean exists = userRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("User with id " + id +" does not exists.");
        }
        userRepository.deleteById(id);
    }

    @Transactional
    public User updateUser(Long id, String name, String email, UserDetails userDetails) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalStateException("User with id " + id + " does not exists."));
        if(user.getRoles().iterator().next().getRole().equals("super_admin")){
            authorityValidator.valudateSuperAdmin(userDetails);
        }
        authorityValidator.valudateAdminAndSuperAdmin(userDetails);

        if(!name.isEmpty() && name != null && !Objects.equals(user.getName(), name)){
            user.setName(name);
        }
        if(!email.isEmpty() && email != null && !Objects.equals(user.getEmail(), email)){
            Optional<User> userByEmail = userRepository.findUserByEmail(email);
            if(userByEmail.isPresent()){
                throw new IllegalStateException("Email already exists.");
            }
            user.setEmail(email);
        }
        return user;
    }

    public User registration(User user) {
        Optional<User> userByEmail = userRepository.findUserByEmail(user.getEmail());
        if(userByEmail.isPresent()){
            throw new IllegalStateException("Email already exists.");
        }
        else if(user.getPassword().isEmpty() || user.getPassword() == null){
            throw new IllegalStateException("Password is required.");
        }
        else if(user.getEmail().isEmpty()){
            throw new IllegalStateException("Email cannot be null.");
        }

        //byte[] passEncoded = Base64.encodeBase64(user.getPassword().getBytes());
        user.setPassword(encodeToBase64(user.getPassword())); //YTEzNTc5MDI0Njg=

        return userRepository.save(user);
    }

    public Set<User> getUserByRole(Role role){
        return userRepository.findByRoles(role);
    }

    public String encodeToBase64(String param){
        byte[] passEncoded = Base64.encodeBase64(param.getBytes());
        return new String(passEncoded);
    }
}
