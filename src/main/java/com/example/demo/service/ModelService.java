package com.example.demo.service;

import com.example.demo.entity.Model;
import com.example.demo.model.ModelModel;
import com.example.demo.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModelService {
    @Autowired
    ModelRepository modelRepository;

    public List<ModelModel> getAll(){
        List<ModelModel> models = new ArrayList<>();
        ModelModel model;
        for (Model m : modelRepository.findAll()) {
            model = new ModelModel(m.getId(), m.getName());
            models.add(model);
        }
        return models;
    }

    public List<Model> getModelstById(List<Long> ids){
        return modelRepository.getModelsById(ids);
    }
}
