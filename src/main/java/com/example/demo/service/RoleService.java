package com.example.demo.service;

import com.example.demo.entity.Role;
import com.example.demo.model.RoleModel;
import com.example.demo.repository.RoleRepository;
import com.example.demo.util.AuthorityValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    AuthorityValidator authorityValidator;

    public List<Role> getRoles(){
        return roleRepository.findAll();
    }

    public List<RoleModel> getRolesAndUsers(UserDetails user){
        authorityValidator.valudateAdminAndSuperAdmin(user);
        List<Role> roles = roleRepository.findAll();
        List<RoleModel> roleModels = new ArrayList<>();
        RoleModel roleModel;
        for (Role role: roles) {
            roleModel = new RoleModel(role.getId(), role.getRole(), userService.getUserByRole(role));
            roleModels.add(roleModel);
        }
        return roleModels;
    }
}
