package com.example.demo.repository;

import com.example.demo.entity.Model;
import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ModelRepository extends JpaRepository<Model, Long> {
    @Query("SELECT e FROM Model e WHERE e.id IN(:ids) ORDER BY e.name")
    List<Model> getModelsById(@Param("ids") List<Long> ids);
}
