package com.example.demo.repository;

import com.example.demo.entity.User;
import com.example.demo.entity.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor {
    @Query("SELECT e FROM User e WHERE e.email LIKE CONCAT('%',:email,'%')")
    List<User> getUsersByEmail(@Param("email") String email);

    @Query("SELECT e FROM User e where CONCAT(e.id, '') LIKE CONCAT('%',:filter,'%') OR LOWER(e.name) LIKE CONCAT('%',:filter,'%') OR LOWER(e.lastName) LIKE CONCAT('%',:filter,'%') OR LOWER(e.username) LIKE CONCAT('%',:filter,'%') OR LOWER(e.email) LIKE CONCAT('%',:filter,'%') OR CONCAT(e.dob,'') LIKE CONCAT('%',:filter,'%')")
    Page<User> getAll(Pageable pageRequest, @Param("filter") String filter);

    @Query("SELECT e FROM User e where CONCAT(e.id, '') LIKE CONCAT('%',:filter,'%') OR LOWER(e.name) LIKE CONCAT('%',:filter,'%') OR LOWER(e.lastName) LIKE CONCAT('%',:filter,'%') OR LOWER(e.username) LIKE CONCAT('%',:filter,'%') OR LOWER(e.email) LIKE CONCAT('%',:filter,'%') OR CONCAT(e.dob,'') LIKE CONCAT('%',:filter,'%')")
    Set<User> exportUsers(Sort sort ,@Param("filter") String filter);

    Optional<User> findUserByEmail(String email);

    User findByEmail(String email);

    User findUserById(Long id);

    Set<User> findByRoles(Role role);

}
