package com.example.demo.views.userView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/view/user")
public class UserViewController {

    @Autowired
    private UserViewService userViewService;

    @GetMapping
    private List<UserView>getUsers(){
        return userViewService.getUsers();
    }
}
