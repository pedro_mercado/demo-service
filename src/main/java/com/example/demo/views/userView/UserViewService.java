package com.example.demo.views.userView;

import com.example.demo.util.AuthorityValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserViewService {
    @Autowired
    UserViewRepository userViewRepository;

    @Autowired
    AuthorityValidator authorityValidator;

    public List<UserView> getUsers(){
        return userViewRepository.findAll();
    }

}
