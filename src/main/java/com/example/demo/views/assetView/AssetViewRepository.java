package com.example.demo.views.assetView;

import com.example.demo.views.userView.UserView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssetViewRepository extends JpaRepository<AssetView, Long> {

}
