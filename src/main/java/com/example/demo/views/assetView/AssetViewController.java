package com.example.demo.views.assetView;

import com.example.demo.views.userView.UserView;
import com.example.demo.views.userView.UserViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/view/asset")
public class AssetViewController {

    @Autowired
    private AssetViewService assetViewService;

    @GetMapping
    private List<AssetView>getAssets(){
        return assetViewService.getAssets();
    }
}
