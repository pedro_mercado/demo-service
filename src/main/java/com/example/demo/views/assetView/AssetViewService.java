package com.example.demo.views.assetView;

import com.example.demo.util.AuthorityValidator;
import com.example.demo.views.userView.UserView;
import com.example.demo.views.userView.UserViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssetViewService {
    @Autowired
    AssetViewRepository assetViewRepository;

    @Autowired
    AuthorityValidator authorityValidator;

    public List<AssetView> getAssets(){
        return assetViewRepository.findAll();
    }

}
