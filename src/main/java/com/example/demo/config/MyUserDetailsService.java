package com.example.demo.config;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.binary.Base64;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException{
        User user = userRepository.findByEmail(userName);
        if(user == null){
            throw new UsernameNotFoundException(userName);
        }
        byte[] passDecoded = Base64.decodeBase64(new String(user.getPassword()));

        UserDetails userToLogin = org.springframework.security.core.userdetails.User.withUsername(user.getEmail()).password(new String(passDecoded)).authorities(user.getRoles().iterator().next().getRole()).build();
        return userToLogin;
    }
}
