package com.example.demo.spec;

import com.example.demo.entity.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class UserSpec {

    private final String wildcard = "%";

    public Specification<User> getFilter(User filters) {
        return (root, query, cb) -> {
            final List<Predicate> filterConditions = new ArrayList<>();
            final List<Predicate> roleConditions = new ArrayList<>();

            query.distinct(true);

            //Since each filter is declared as a class property need
            //to use reflection to get the list and be able to iterate
            Field[] fields = filters.getClass().getDeclaredFields();

            //Iterating over each property
            for(Field f : fields){
                f.setAccessible(true);
                Class fieldType = f.getType();
                Object v = null;

                try {
                    v = f.get(filters);
                } catch (IllegalAccessException e) {
                    System.out.println(e);
                }

                if(v != null) {
                    // If the filter is String
                    if (fieldType == String.class) {
                        v = v.toString().replace("\\", "\\\\").replace("%", "\\%");
                            filterConditions.add(cb.like(cb.lower(root.get(f.getName())), containsLowerCase(v.toString())));
                    }
                    //If the filter es Long
                    else if (fieldType == Long.class) {
                        switch (f.getName()){
                            case "id" : {
                                filterConditions.add(cb.equal(root.get("id"), new BigInteger(String.valueOf(v))));
                                break;
                            }
                            default : {
                                filterConditions.add(cb.equal(root.get(f.getName()), new BigInteger(String.valueOf(v))));
                            }
                        }
                    }
                        //If the filter is Integer
                        else if (fieldType == Integer.class) {
                            filterConditions.add(cb.equal(root.get(f.getName()), v));
                        }
                    //If the filter is BigInteger
                    else if (fieldType == BigInteger.class) {
                        filterConditions.add(cb.like(root.get(f.getName()).as(String.class), "%" + v.toString() + "%"));
                    }
                    //If the filter is Integer
                    else if (fieldType == LocalDate.class) {
                        try{
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                            LocalDate date = LocalDate.parse(v.toString(), formatter);
                            filterConditions.add(cb.equal(root.get("dob"), date));
                        } catch (Exception e){

                        }
                    }
                    //If the filter es boolean
                    /*else if (fieldType == Boolean.class) {
                        if(f.getName().equals("filterPackage")){
                            if(v.equals(true)){
                                filterConditions.add(cb.greaterThan(root.get("packCount"), 0));
                            }
                            else if(v.equals(false)){
                                filterConditions.add(cb.equal(root.get("packCount"), 0));
                            }
                        }
                        else if(f.getName().equals("project")){
                            if(v.equals(false)) {
                                Predicate p1 = cb.isFalse(root.get(f.getName()));
                                Predicate p2 = cb.or(root.get(f.getName()).isNull());
                                Predicate or = cb.or(p1, p2);
                                filterConditions.add(or);
                            }
                            else if(v.equals(true)) {
                                filterConditions.add(cb.isTrue(root.get(f.getName())));
                            }
                        }
                        else{
                            filterConditions.add(cb.equal(root.get(f.getName()), v));
                        }
                    }*/
                }
            }

            Specification<User> dynamicSpec = (root1, criteriaQuery, criteriaBuilder) -> null;

            //No role conditions if it's a full admin
            if(roleConditions.size() > 0 && filterConditions.size() > 0){
                query.where(
                        cb.or(roleConditions.toArray(new Predicate[]{})),
                        cb.and(filterConditions.toArray(new Predicate[]{}))
                );
            }else if (roleConditions.size() > 0 && filterConditions.size() == 0){
                query.where(
                        cb.or(roleConditions.toArray(new Predicate[]{}))
                );
            }else if (roleConditions.size() == 0 && filterConditions.size() > 0){
                query.where(
                        cb.and(filterConditions.toArray(new Predicate[]{}))
                );
            }

            return dynamicSpec.toPredicate(root,query,cb);
        };
    }

    public Specification<User> getGlobalFilter(String globalFilter) {
        globalFilter = globalFilter.replace("\\", "\\\\").replace("%", "\\%");

        String finalGlobalFilter = globalFilter;

        return (root, query, cb) -> {
            final List<Predicate> filterConditions = new ArrayList<>();
            final List<Predicate> roleConditions = new ArrayList<>();

            query.distinct(true);

            if(finalGlobalFilter.chars().allMatch(Character::isDigit)){
                filterConditions.add(cb.equal(root.get("id"), new BigInteger(finalGlobalFilter)));
            }
            filterConditions.add(cb.or(cb.like(cb.lower(root.get("name")), containsLowerCase(finalGlobalFilter))));
            filterConditions.add(cb.or(cb.like(cb.lower(root.get("lastName")), containsLowerCase(finalGlobalFilter))));
            filterConditions.add(cb.or(cb.like(cb.lower(root.get("username")), containsLowerCase(finalGlobalFilter))));
            filterConditions.add(cb.or(cb.like(cb.lower(root.get("email")), containsLowerCase(finalGlobalFilter))));

            //filterConditions.add(cb.or(cb.isMember(finalGlobalFilter,root.get("role").get("role"))));

            try{
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDate date = LocalDate.parse(finalGlobalFilter, formatter);
                filterConditions.add(cb.or(cb.equal(root.get("dob"), date)));
            } catch (Exception e){

            }

            //TODO ADD due_date FILTER

            Specification<User> dynamicSpec = (root1, criteriaQuery, criteriaBuilder) -> null;

            //No role conditions if it's a full admin
            if(roleConditions.size() > 0 && filterConditions.size() > 0){
                query.where(
                        cb.or(roleConditions.toArray(new Predicate[]{})),
                        cb.or(filterConditions.toArray(new Predicate[]{}))
                );
            }else if (roleConditions.size() > 0 && filterConditions.size() == 0){
                query.where(
                        cb.or(roleConditions.toArray(new Predicate[]{}))
                );
            }else if (roleConditions.size() == 0 && filterConditions.size() > 0){
                query.where(
                        cb.or(filterConditions.toArray(new Predicate[]{}))
                );
            }

            return dynamicSpec.toPredicate(root,query,cb);
        };
    }

    protected String containsLowerCase(String searchField) {
        return wildcard + searchField.toLowerCase() + wildcard;
    }
}
