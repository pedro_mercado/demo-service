package com.example.demo;

import com.example.demo.service.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class DemoApplicationTests {

	@Autowired
	UserService userService;

	private String str;

	@BeforeAll
	static void beforeAll(){
		System.out.println("Initialize database connection.");
	}

	@AfterAll
	static void afterAll(){
		System.out.println("Closing database connection.");
	}

	@BeforeEach
	void beforeEach(TestInfo info){
		System.out.println("Before each test: " + info.getDisplayName());
	}

	@AfterEach
	void afterEach(TestInfo info){
		System.out.println("After each test: " + info.getDisplayName());
	}

	@Test
	void testLogin(){
		String str = "AAB,C";
		str = str.replace("A","");
		System.out.println(str);
		assertEquals("B,C", str);

		String[] splitted = str.split(",");
		String[] expected = {"B","C"};
		assertArrayEquals(expected, splitted);
		Arrays.stream(splitted).forEach(c-> {
			System.out.println(c);
		});

	}

	@Test
	void contextLoads() {
		int length = "ABCD".length();
		int expectedLength = 4;

		assertEquals(expectedLength, length);
	}

	@Test
	void toUpperCase(){
		String str = "abcd";
		String result = str.toUpperCase();
		assertNotNull(result);
		assertEquals("ABCD", result);
	}

	@Test
	void contains_basic(){
		String str = "abcdefg";
		assertFalse(str.contains("fgs"));
	}

	@Test
	void split() {
		String str = "abc def ghi";
		String splited[] = str.split(" ");
		String[] expectedString = {"abc", "def", "ghi"};
		assertArrayEquals(expectedString, splited);
	}

	@Test
	void lengthException() {
		String str = null;
		assertThrows(NullPointerException.class, () ->{
			int length = str.length();
			System.out.println("Length: " + length);
		});
	}

	@Test
	@RepeatedTest(5)
	void lengthGreaterZero(){
		assertTrue("ABCD".length() > 0);
		assertTrue("ABC".length() > 0);
		assertTrue("A".length() > 0);
		assertTrue("DEF".length() > 0);
	}

	@ParameterizedTest
	@ValueSource(strings = {"ABCD", "ABC", "A", "DEF"})
	void lengthGreaterZeroParameters(String str){
		assertTrue(str.length() > 0);
	}

	@Test
	@Disabled
	void performanceTest(){
		assertTimeout(Duration.ofSeconds(5), () ->{
			for(int i = 0; i < 1000000; i++){
				int j = i;
				System.out.println(j);
			}
		});
	}

	@Nested
	class EmptyStringTest{
		@BeforeEach
		void setToEmpty(){
			str = "";
		}

		@Test
		void upperCaseIsEmpty(){
			assertEquals("", str.toUpperCase());
		}
	}


}
